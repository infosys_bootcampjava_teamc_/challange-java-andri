import java.util.Scanner;

public class segitiga {
    static void luasSegitiga(){
        Scanner scan = new Scanner(System.in);

        int alas, tinggi;
        double luas; //karena alas itu nilai nya 0.5

        System.out.println("===================================");
        System.out.println("|       Anda Memilih Segitiga     |");
        System.out.println("===================================");
        System.out.print(" Masukkan Alas   : ");
        alas = scan.nextInt();
        System.out.print(" Masukkan Tinggi : ");
        tinggi = scan.nextInt();
        luas = 0.5 * alas * tinggi; //Rumus Luas Segitiga
        System.out.println("==================================");
        System.out.println(" Luas segitiga adalah : " + luas);
    }
}

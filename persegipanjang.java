import java.util.Scanner;

    public class persegipanjang {
        static void luasPersegiPanjang() {
            //Scanner inputan
            Scanner input = new Scanner(System.in);

            //Variabel persegi panjang
            int panjang, lebar;
            System.out.println("====================================");
            System.out.println("|   Anda Memilih Persegi Panjang  |");
            System.out.println("=====================================");
            System.out.print(" Masukkan Panjang Persegi : ");
            panjang = input.nextInt(); //Inputan Panjang Persegi
            System.out.print(" Masukkan Lebar Persegi   : ");
            lebar = input.nextInt(); //Inputan Lebar Persegi
            System.out.println("======================================");
            System.out.println(" Luas persegi panjang adalah : " + panjang * lebar);
        }
    }


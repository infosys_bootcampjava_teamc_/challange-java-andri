import java.util.Scanner;

public class Tabung {
    static void volumeTabung(){
        Scanner scan = new Scanner(System.in);

        double volume, r, tinggi;
        double phi = 3.14;

        System.out.println("===================================");
        System.out.println("|         Anda Memilih Balok      |");
        System.out.println("===================================");
        System.out.print(" Masukkan Nilai r Tabung   : ");
        r = scan.nextInt();
        System.out.print(" Masukkan Tinggi Tabung    : ");
        tinggi = scan.nextInt();
        volume = (phi * r * r) * tinggi;
        System.out.println("==================================");
        System.out.println(" Volume tabung adalah : " + volume);
    }
}

import java.util.Scanner;

    public class Kubus {
        static void volumeKubus(){
            Scanner scan = new Scanner(System.in);

            int sisi , volume;

            System.out.println("===================================");
            System.out.println("|         Anda Memilih Kubus      |");
            System.out.println("===================================");
            System.out.print(" Masukkan Sisi Kubus   : ");
            sisi = scan.nextInt();
            volume = sisi * sisi * sisi; //Rumus Volume Kubus
            System.out.println("==================================");
            System.out.println(" Volume kubus adalah : " + volume);
        }
}

import java.util.Scanner;

public class Balok {
    static void volumeBalok(){
            Scanner scan = new Scanner(System.in);

            int panjang, lebar, tinggi , volume;

            System.out.println("===================================");
            System.out.println("|         Anda Memilih Balok      |");
            System.out.println("====================================");
            System.out.print(" Masukkan Panjang Balok   : ");
            panjang = scan.nextInt();
            System.out.print(" Masukkan Lebar Balok   : ");
            lebar = scan.nextInt();
            System.out.print(" Masukkan Tinggi Balok   : ");
            tinggi = scan.nextInt();
            volume = panjang * lebar * tinggi; //Rumus Volume Balok
            System.out.println("====================================");
            System.out.println(" Volume balok adalah : " + volume);
        }
}

import java.util.Scanner;

public class Lingkaran {
    static void luasLingkaran(){
        Scanner scan = new Scanner(System.in);
        double luas, //Tipe data nya Double karena ada bilangan desimal
                phi=3.14;
        int r;

        System.out.println("===================================");
        System.out.println("|      Anda Memilih Lingkaran     |");
        System.out.println("===================================");
        System.out.print(" Masukkan Jari-Jari (r) = ");
        r = scan.nextInt();
        luas = phi*r*r;
        System.out.println("===================================");
        System.out.println(" Luas lingkaran adalah : " + luas);
    }
}

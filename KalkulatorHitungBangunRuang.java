import java.util.Scanner;

public class KalkulatorHitungBangunRuang {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int pilihan;
        System.out.println("==================================");
        System.out.println("|   Kalkulator luas dan Volume   |");
        System.out.println("===================================");
        System.out.println(" Menu ");
        System.out.println(" 1. Hitung Luas Bidang ");
        System.out.println(" 2. Hitung Volume ");
        System.out.println(" 0. Tutup Aplikasi ");
        System.out.println("===================================");
        pilihan = scan.nextInt();

        switch (pilihan){
            case 1:
                System.out.println("===================================");
                System.out.println("| Pilih Bidang Yang Akan Dihitung |");
                System.out.println("====================================");
                System.out.println(" 1. Persegi ");
                System.out.println(" 2. Lingkaran ");
                System.out.println(" 3. Segitiga ");
                System.out.println(" 4. Persegi Panjang ");
                System.out.println(" 0. Kembali Ke Halaman Pertama ");
                System.out.println("====================================");
                pilihan = scan.nextInt();

                switch (pilihan){
                    case 1:
                        Persegi.luasPersegi();
                        break;
                    case 2:
                        Lingkaran.luasLingkaran();
                        break;
                    case 3:
                        segitiga.luasSegitiga();
                        break;
                    case 4:
                        persegipanjang.luasPersegiPanjang();
                        break;
                    default:
                        //System.exit(0);
                        System.out.println("Salah Pilih ! :(");
                }

                break;
            case 2:
                System.out.println("=========================================");
                System.out.println("| Pilih Bangun Ruang Yang Akan Dihitung |");
                System.out.println("=========================================");
                System.out.println(" 1. Kubus ");
                System.out.println(" 2. Balok ");
                System.out.println(" 3. Tabung ");
                System.out.println(" 0. Kembali Ke Halaman Pertama ");
                System.out.println("---------------------------------------");

                pilihan = scan.nextInt();
                switch (pilihan){
                    case 1:
                        Kubus.volumeKubus();
                        break;
                    case 2:
                        Balok.volumeBalok();
                        break;
                    case 3:
                        Tabung.volumeTabung();
                        break;
                    default:
                        //System.exit(0);
                        System.out.println("Salah Pilih ! :(");
                }
                break;
            default:
//                System.exit(0);
                System.out.println("Salah Pilih ! :(");
        }
    }
}
